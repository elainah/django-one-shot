from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list_list": todolist,
    }
    return render(request, "todolists/list.html", context)


def todo_list_detail(request, id):
    todoitemslist = get_object_or_404(TodoList, id=id)
    context = {
        "todoitemslist": todoitemslist,
    }
    return render(request, "todolists/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todolists/create.html", context)


def todo_list_update(request, id):
    todoitemslist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todoitemslist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todoitemslist.id)
    else:
        form = TodoListForm(instance=todoitemslist)

    context = {
        "form": form,
    }
    return render(request, "todolists/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todolists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todolists/create_item.html", context)


def todo_item_update(request, id):
    todoitemstasks = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitemstasks)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todoitemstasks.id)
    else:
        form = TodoItemForm(instance=todoitemstasks)

    context = {
        "form": form,
    }
    return render(request, "todolists/edit_item.html", context)
